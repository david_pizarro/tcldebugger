%{!?tcl_version: %define tcl_version %(echo 'puts $tcl_version' | tclsh)}
%{!?tcl_sitelib: %define tcl_sitelib %{_datadir}/tcl%{tcl_version}}
%define build_date %(date +"%Y%m%d")

Summary: Tcl debugging library
Name: tcldebugger
Version: 1.4
Release: 10.Pizarro.1.%{build_date}%{?dist}
License: TCL
Group: Development/Libraries
Source0: tcldebugger_src
Source1: config.sub
Source2: config.guess
Patch0:  tcldebugger-1.4-tclm4.patch
Patch1:  tcldebugger-1.4-destdir.patch
Patch2:  tcldebugger-1.4-tkupdate.patch
Patch3:  tcldebugger-1.4-shortsplash.patch
Patch4:  tcldebugger-1.4-shebang.patch
Patch5:  tcldebugger-1.4-browser.patch
# Upstream URL: http://tclpro.sourceforge.net/
URL: http://bitbucket.org/david_pizarro/tcldebugger
#BuildRequires: tcl-devel autoconf
BuildRequires: tcl autoconf
Requires: tcl(abi) = 8.5 tk tclparser tcllib xdg-utils
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%description
A debugging library for Tcl.  This is used by the graphical TclPro debugger.

%prep
# clean and prep the build directory
rm -rf *
cp -a %{SOURCE0}/* .
%patch0 -p0
%patch1 -p0
%patch2 -p0
%patch3 -p0
%patch4 -p0
%patch5 -p1
autoconf
cp -a %{SOURCE1} %{SOURCE2} config/

%build
%configure --libdir=%{tcl_sitelib} --datadir=%{tcl_sitelib}
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

# Fix a few file permissions for rpmlint
chmod a-x $RPM_BUILD_ROOT%{tcl_sitelib}/%{name}%{version}/*.tcl
chmod a-x $RPM_BUILD_ROOT%{tcl_sitelib}/%{name}%{version}/*.pdx

%check
# Disabling unit tests as they require tools that haven't been built yet.
#make test

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc ChangeLog license.terms
%defattr(-,root,root)
%{_bindir}/initdebug.tcl
%{_bindir}/appLaunch.tcl
%{tcl_sitelib}/%{name}%{version}
%{_mandir}/mann/*.gz

%changelog
* Mon Nov 17 2014 Pizarro <david.pizarro@gmail.com> - 1.4-10.Pizarro.0.20141117
- Initial fork, no functional change

* Sun Jul 26 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4-10.20061030cvs
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.4-9.20061030cvs
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Fri Jun 13 2008 Wart <wart at kobold.org> 1.4-8.20061030cvs
- Drop dependency on tbcload which does not work with Tcl 8.5

* Thu Jun 4 2008 Wart <wart at kobold.org> 1.4-7.20061030cvs
- Update autoconf support files to known working versions (BZ #449464)

* Thu Jan 3 2008 Wart <wart at kobold.org> 1.4-6.20061030cvs
- Rebuild for Tcl 8.5

* Mon Oct 1 2007 Wart <wart at kobold.org> 1.4-5.20061030cvs
- Switch from htmlview to xdg-utils

* Tue Aug 21 2007 Wart <wart at kobold.org> 1.4-4.20061030cvs
- License tag clarification
- Apply forgotten patch

* Sun Jun 3 2007 Wart <wart at kobold.org> 1.4-3.20061030cvs
- Change Help menu command from 'netscape' to the generic 'htmlview'
  and add 'Requires: htmlview'

* Sat Jun 2 2007 Wart <wart at kobold.org> 1.4-2.20061030cvs
- Move to a tcl-specific directory for faster loading

* Tue Oct 31 2006 Wart <wart at kobold.org> 1.4-1.20061030cvs
- Initial package for Fedora
